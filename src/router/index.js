import Router from 'vue-router'
import Thumbnails from '../components/Thumbnails.vue'
import VideoViewer from '../components/VideoViewer'
import Vue from 'vue'

Vue.use(Router)


export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Thumbnails
    },
    {
      path: '/video/:videoId',
      component: VideoViewer
    }
  ]
})
