import Vue from 'vue'
// import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/',
    component: import('./components/Thumbnails.vue')
  }]
})

new Vue({
  el: '#app',
  render: h => h(require('./App.vue')),
  router
})
